# Docker python eclipse

Docker container defining a development environment for python with eclipse.

## Build
$docker build devenv

## Run
$docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY --user $UID -v $HOME:$HOME -e HOME=$HOME -w $HOME/workspace devenv /opt/eclipse/eclipse