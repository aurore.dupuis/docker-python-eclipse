FROM ubuntu:18.04

# Install python and gcc etc
RUN apt-get update && apt-get install -y \
    software-properties-common \
    python2.7 \
    python-pip \
    build-essential \
    libssl-dev libffi-dev python-dev \
	openjdk-8-jre -y \
	tar wget gzip -y \
    && rm -rf /var/lib/apt/lists/* \

# Install virtual env
 && pip install --no-cache-dir virtualenv \

# Install pylint
 && pip install --no-cache-dir pylint \

# Install Eclipse
&& wget --debug -O eclipse.tar.gz \
        'https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/3/eclipse-cpp-oxygen-3-linux-gtk-x86_64.tar.gz&r=1' && \
    tar xv -C /opt -f eclipse.tar.gz && \
    rm -vf eclipse.tar.gz \

# Install PyDev
&& /opt/eclipse/eclipse -nosplash \
                         -application org.eclipse.equinox.p2.director \
                         -destination /opt/eclipse \
                         -repository http://download.eclipse.org/technology/dltk/updates/,http://pydev.org/updates/ \
                         -installIU org.python.pydev.feature.feature.group \
&& chmod -R g-w /opt/eclipse

